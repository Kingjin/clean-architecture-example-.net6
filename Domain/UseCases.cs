﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Варианты использования (сервисы)
    /// </summary>
    public class UseCases
    {
        /// <summary>
        /// Получить название книги по её ID
        /// </summary>
        /// <param name="bookID"></param>
        /// <returns></returns>
        public static string? GetBookTitle(int bookID)
        {
            if (Settings.Repository == null) {
                throw new Exception("Репозиторий не задан");                
            }

            var book = Settings.Repository.GetBook(bookID);
            if (book == null)
            {
                return null;
            }

            return book.Title;
        }
    }
}
