﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Настройки
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Репозиторий
        /// </summary>
        public static IRepository? Repository { get; set; }
    }
}
