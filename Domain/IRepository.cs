﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Интерфейс Репозитория
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Получить книгу по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entities.Book? GetBook(int id);
    }
}
