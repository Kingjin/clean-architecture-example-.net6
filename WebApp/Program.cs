Domain.Settings.Repository = new PgRepository.Repository();

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () => "Clean Architecture example");

app.MapPost("/get_book_title", WebApp.Routes.GetBookTitle);

app.Run();