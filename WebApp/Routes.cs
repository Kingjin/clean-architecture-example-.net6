﻿namespace WebApp
{
    public record RecID (
        int ID
    );

    public class Routes
    {
        public static object? GetBookTitle(RecID recID)
        {
            return new { title = Domain.UseCases.GetBookTitle(recID.ID) };
        }
    }
}
