﻿

namespace PgRepository
{
    /// <summary>
    /// Репозиторий
    /// </summary>
    public class Repository : Domain.IRepository
    {
        private Context _context = new Context();

        public Domain.Entities.Book? GetBook(int id) {
            // Получение книги из БД
            var book = _context.Books.Find(id);
            if (book == null)
            {
                return null;
            }

            // Маппинг на сущность-книгу Домена
            var dmBook = new Domain.Entities.Book
            {
                ID = book.ID,
                Title = book.Title
            };
            return dmBook;  
        }
    }
}