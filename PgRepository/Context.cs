﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace PgRepository
{
    internal class Context: DbContext
    {
        public DbSet<Models.Book> Books { get; set; } 

        public Context()
        {
            //! Только для тестирования, не для Production            
            Database.EnsureDeleted();  // Удаление БД
            Database.EnsureCreated();  // Создание БД

            // Заполнение БД
            Books.Add(new Models.Book { Title = "Clean Architecture" });
            Books.Add(new Models.Book { Title = "Domain-Driven Design: Tackling Complexity in the Heart of Software" });
            Books.Add(new Models.Book { Title = "Clean Code: A Handbook of Agile Software Craftsmanship" });
            SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //! Только для тестирования, не для Production
            // ConnectionString вынести в конфигурационный файл
            optionsBuilder.UseNpgsql(@"Host=localhost;Port=5432;Database=library-test;Username=postgres;Password=postgres");
        }
    }
}
