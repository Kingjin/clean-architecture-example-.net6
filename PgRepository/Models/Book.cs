﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PgRepository.Models
{
    /// <summary>
    /// Книга
    /// </summary>
    [Table("books")]
    public class Book
    {
        [Key, Column("id")]
        public int ID { get; set; }

        [Required, Column("title")]
        public string? Title { get; set; }
    }
}
